package com.realwear.blekeyboard.ui.main

import android.app.Activity
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.os.Bundle
import android.os.ParcelUuid
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.king.keyboard.KeyboardView
import com.king.keyboard.KingKeyboard
import com.realwear.blekeyboard.MainActivity
import com.realwear.blekeyboard.R
import com.realwear.blekeyboard.ServiceFragment
import java.lang.StringBuilder
import java.util.*
import kotlin.experimental.and

class MainFragment : ServiceFragment() {

    private lateinit var editText: EditText
    private var mHeartRateControlPoint: BluetoothGattCharacteristic

    private lateinit var kingKeyboard: KingKeyboard
    private val BATTERY_SERVICE_UUID = UUID
        .fromString("f4e31a3a-c2fa-11eb-8529-0242ac130003")

    private val BATTERY_LEVEL_UUID = UUID
        .fromString("0bd0646e-c2fb-11eb-8529-0242ac130003")

    private val HEART_RATE_CONTROL_POINT_UUID = UUID
        .fromString("00002A39-0000-1000-8000-00805f9b34fb")

    private val EXPENDED_ENERGY_FORMAT = BluetoothGattCharacteristic.FORMAT_UINT16

    private val INITIAL_BATTERY_LEVEL = 50
    private val BATTERY_LEVEL_MAX = 100
    private val BATTERY_LEVEL_DESCRIPTION = "The current charge level of a " +
            "battery. 100% represents fully charged while 0% represents fully discharged."

    private val INITIAL_HEART_RATE_MEASUREMENT_VALUE = 60
    private val INITIAL_EXPENDED_ENERGY = 0

    private var mDelegate: ServiceFragmentDelegate? = null

    // GATT
    private var mBatteryService: BluetoothGattService
    private var mBatteryLevelCharacteristic: BluetoothGattCharacteristic

    private var stringBuilder = StringBuilder()

    companion object {
        fun newInstance() = MainFragment()
    }

    init {
        mBatteryLevelCharacteristic = BluetoothGattCharacteristic(
            BATTERY_LEVEL_UUID,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_NOTIFY,
            BluetoothGattCharacteristic.PERMISSION_READ
        )

        mBatteryLevelCharacteristic.addDescriptor(
            MainActivity.getClientCharacteristicConfigurationDescriptor()
        )

        mBatteryLevelCharacteristic.addDescriptor(
            MainActivity.getCharacteristicUserDescriptionDescriptor(BATTERY_LEVEL_DESCRIPTION)
        )

        mBatteryService = BluetoothGattService(
            BATTERY_SERVICE_UUID,
            BluetoothGattService.SERVICE_TYPE_PRIMARY
        )


        mHeartRateControlPoint = BluetoothGattCharacteristic(
            HEART_RATE_CONTROL_POINT_UUID,
            BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_WRITE
        )

        mBatteryService.addCharacteristic(mBatteryLevelCharacteristic)
        mBatteryService.addCharacteristic(mHeartRateControlPoint)
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        kingKeyboard = KingKeyboard(requireActivity(), view.findViewById(R.id.keyboard_main_container))
        editText = view.findViewById(R.id.edit_text) as EditText
        kingKeyboard.register(editText, KingKeyboard.KeyboardType.NORMAL)

        kingKeyboard.setOnKeyboardActionListener(object : KeyboardView.OnKeyboardActionListener {
            override fun onPress(primaryCode: Int) {

            }

            override fun onRelease(primaryCode: Int) {

            }

            override fun onKey(primaryCode: Int, keyCodes: IntArray?) {
                setBatteryLevel(primaryCode)
            }

            override fun onText(text: CharSequence?) {

            }

            override fun swipeLeft() {

            }

            override fun swipeRight() {

            }

            override fun swipeDown() {

            }

            override fun swipeUp() {

            }

        })
    }

    private fun setBatteryLevel(keyCode: Int) {
        mBatteryLevelCharacteristic.setValue(
            keyCode,
            BluetoothGattCharacteristic.FORMAT_UINT8,  /* offset */0
        )
        mDelegate!!.sendNotificationToDevices(mBatteryLevelCharacteristic)
    }

    override fun notificationsEnabled(
        characteristic: BluetoothGattCharacteristic,
        indicate: Boolean
    ) {
        if (characteristic.uuid !== BATTERY_LEVEL_UUID) {
            return
        }
        if (indicate) {
            return
        }
        requireActivity().runOnUiThread {
            Toast.makeText(activity, "Notifications enabled", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun notificationsDisabled(characteristic: BluetoothGattCharacteristic) {
        if (characteristic.uuid !== BATTERY_LEVEL_UUID) {
            return
        }
        requireActivity().runOnUiThread {
            Toast.makeText(activity, "Notifications not enabled", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onResume() {
        super.onResume()
        kingKeyboard.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        kingKeyboard.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun getBluetoothGattService(): BluetoothGattService {
        return mBatteryService
    }

    override fun getServiceUUID(): ParcelUuid {
        return ParcelUuid(BATTERY_SERVICE_UUID)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mDelegate = try {
            activity as ServiceFragmentDelegate
        } catch (e: ClassCastException) {
            throw ClassCastException(
                activity.toString()
                        + " must implement ServiceFragmentDelegate"
            )
        }
    }


    override fun writeCharacteristic(
        characteristic: BluetoothGattCharacteristic?,
        offset: Int,
        value: ByteArray
    ): Int {
        if (offset != 0) {
            return BluetoothGatt.GATT_INVALID_OFFSET
        }
        // Heart Rate control point is a 8bit characteristic
//        if (value.size != 1) {
//            return BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH
//        }
//        if ((value[0] and 1) as Int == 1) {
            requireActivity()!!.runOnUiThread {
                mBatteryLevelCharacteristic?.setValue(
                    INITIAL_EXPENDED_ENERGY,
                    EXPENDED_ENERGY_FORMAT,  /* offset */
                    2
                )
//                mEditTextEnergyExpended.setText(Integer.toString(io.github.webbluetoothcg.bletestperipheral.HeartRateServiceFragment.INITIAL_EXPENDED_ENERGY))
//                Toast.makeText(requireContext(), "HeartRate :: " + Integer.toString(INITIAL_EXPENDED_ENERGY), Toast.LENGTH_SHORT).show()
                stringBuilder.append(String(value))
                editText.setText(stringBuilder.toString())
            }
//        }
        return BluetoothGatt.GATT_SUCCESS
    }

    override fun onDetach() {
        super.onDetach()
        mDelegate = null
    }

}