package com.realwear.blekeyboard

import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.realwear.blekeyboard.ui.main.MainFragment
import java.util.*

class MainActivity : AppCompatActivity(), ServiceFragment.ServiceFragmentDelegate {

    private var mAdvertiser: BluetoothLeAdvertiser? = null
    private lateinit var mCurrentServiceFragment: ServiceFragment
    private var mGattServer: BluetoothGattServer? = null
    private lateinit var mAdvScanResponse: AdvertiseData
    private lateinit var mAdvData: AdvertiseData
    private lateinit var mAdvSettings: AdvertiseSettings
    private lateinit var mBluetoothGattService: BluetoothGattService
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private lateinit var mBluetoothManager: BluetoothManager
    private lateinit var mBluetoothDevices: HashSet<BluetoothDevice>


    private val mGattServerCallback: BluetoothGattServerCallback =
        object : BluetoothGattServerCallback() {
            override fun onConnectionStateChange(
                device: BluetoothDevice,
                status: Int,
                newState: Int
            ) {
                super.onConnectionStateChange(device, status, newState)
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (newState == BluetoothGatt.STATE_CONNECTED) {
                        mBluetoothDevices.add(device)
//                        updateConnectedDevicesStatus()
                        Toast.makeText(this@MainActivity, "Device connected", Toast.LENGTH_LONG).show()
                        Log.v(
                            TAG,
                            "Connected to device: " + device.address
                        )
                    } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                        mBluetoothDevices.remove(device)
//                        updateConnectedDevicesStatus()
                        Toast.makeText(this@MainActivity, "Device disconnected", Toast.LENGTH_LONG).show()
                        Log.v(
                            TAG,
                            "Disconnected from device"
                        )
                    }
                } else {
                    mBluetoothDevices.remove(device)
//                    updateConnectedDevicesStatus()
                    Toast.makeText(this@MainActivity, "Device disconnected", Toast.LENGTH_LONG).show()
                    // There are too many gatt errors (some of them not even in the documentation) so we just
                    // show the error to the user.
                    val errorMessage =
                        getString(R.string.status_errorWhenConnecting) + ": " + status
                    runOnUiThread {
                        Toast.makeText(this@MainActivity, errorMessage, Toast.LENGTH_LONG).show()
                    }
                    Log.e(
                        TAG,
                        "Error when connecting: $status"
                    )
                }
            }

            override fun onCharacteristicReadRequest(
                device: BluetoothDevice, requestId: Int, offset: Int,
                characteristic: BluetoothGattCharacteristic
            ) {
                super.onCharacteristicReadRequest(device, requestId, offset, characteristic)
                Log.d(
                    TAG,
                    "Device tried to read characteristic: " + characteristic.uuid
                )
                Log.d(
                    TAG,
                    "Value: " + Arrays.toString(characteristic.value)
                )
                if (offset != 0) {
                    mGattServer!!.sendResponse(
                        device,
                        requestId,
                        BluetoothGatt.GATT_INVALID_OFFSET,
                        offset,  /* value (optional) */
                        null
                    )
                    return
                }
                mGattServer!!.sendResponse(
                    device, requestId, BluetoothGatt.GATT_SUCCESS,
                    offset, characteristic.value
                )
            }

            override fun onNotificationSent(device: BluetoothDevice, status: Int) {
                super.onNotificationSent(device, status)
                Log.v(
                    TAG,
                    "Notification sent. Status: $status"
                )
            }

            override fun onCharacteristicWriteRequest(
                device: BluetoothDevice,
                requestId: Int,
                characteristic: BluetoothGattCharacteristic,
                preparedWrite: Boolean,
                responseNeeded: Boolean,
                offset: Int,
                value: ByteArray
            ) {
                super.onCharacteristicWriteRequest(
                    device, requestId, characteristic, preparedWrite,
                    responseNeeded, offset, value
                )
                Log.v(
                    TAG,
                    "Characteristic Write request: " + Arrays.toString(value)
                )
                val status: Int =
                    mCurrentServiceFragment.writeCharacteristic(characteristic, offset, value)
                if (responseNeeded) {
                    mGattServer!!.sendResponse(
                        device, requestId, status,  /* No need to respond with an offset */
                        0,  /* No need to respond with a value */
                        null
                    )
                }
            }

            override fun onDescriptorReadRequest(
                device: BluetoothDevice, requestId: Int,
                offset: Int, descriptor: BluetoothGattDescriptor
            ) {
                super.onDescriptorReadRequest(device, requestId, offset, descriptor)
                Log.d(
                    TAG,
                    "Device tried to read descriptor: " + descriptor.uuid
                )
                Log.d(
                    TAG,
                    "Value: " + Arrays.toString(descriptor.value)
                )
                if (offset != 0) {
                    mGattServer!!.sendResponse(
                        device,
                        requestId,
                        BluetoothGatt.GATT_INVALID_OFFSET,
                        offset,  /* value (optional) */
                        null
                    )
                    return
                }
                mGattServer!!.sendResponse(
                    device, requestId, BluetoothGatt.GATT_SUCCESS, offset,
                    descriptor.value
                )
            }

            override fun onDescriptorWriteRequest(
                device: BluetoothDevice,
                requestId: Int,
                descriptor: BluetoothGattDescriptor,
                preparedWrite: Boolean,
                responseNeeded: Boolean,
                offset: Int,
                value: ByteArray
            ) {
                super.onDescriptorWriteRequest(
                    device, requestId, descriptor, preparedWrite, responseNeeded,
                    offset, value
                )
                Log.v(
                    TAG,
                    "Descriptor Write Request " + descriptor.uuid + " " + Arrays.toString(value)
                )
                var status = BluetoothGatt.GATT_SUCCESS
                if (descriptor.uuid === CLIENT_CHARACTERISTIC_CONFIGURATION_UUID) {
                    val characteristic = descriptor.characteristic
                    val supportsNotifications = characteristic.properties and
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0
                    val supportsIndications = characteristic.properties and
                            BluetoothGattCharacteristic.PROPERTY_INDICATE != 0
                    if (!(supportsNotifications || supportsIndications)) {
                        status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED
                    } else if (value.size != 2) {
                        status = BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH
                    } else if (Arrays.equals(
                            value,
                            BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
                        )
                    ) {
                        status = BluetoothGatt.GATT_SUCCESS
                        mCurrentServiceFragment.notificationsDisabled(characteristic)
                        descriptor.value = value
                    } else if (supportsNotifications &&
                        Arrays.equals(value, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                    ) {
                        status = BluetoothGatt.GATT_SUCCESS
                        mCurrentServiceFragment.notificationsEnabled(
                            characteristic,
                            false /* indicate */
                        )
                        descriptor.value = value
                    } else if (supportsIndications &&
                        Arrays.equals(value, BluetoothGattDescriptor.ENABLE_INDICATION_VALUE)
                    ) {
                        status = BluetoothGatt.GATT_SUCCESS
                        mCurrentServiceFragment.notificationsEnabled(
                            characteristic,
                            true /* indicate */
                        )
                        descriptor.value = value
                    } else {
                        status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED
                    }
                } else {
                    status = BluetoothGatt.GATT_SUCCESS
                    descriptor.value = value
                }
                if (responseNeeded) {
                    mGattServer!!.sendResponse(
                        device, requestId, status,  /* No need to respond with offset */
                        0,  /* No need to respond with a value */
                        null
                    )
                }
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        mBluetoothDevices = HashSet<BluetoothDevice>()
        mBluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = mBluetoothManager.getAdapter()

        mCurrentServiceFragment = MainFragment.newInstance() as ServiceFragment
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, mCurrentServiceFragment)
                .commitNow()
        }

        mBluetoothGattService = mCurrentServiceFragment.getBluetoothGattService()

        mAdvSettings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
            .setConnectable(true)
            .build()
        mAdvData = AdvertiseData.Builder()
            .setIncludeTxPowerLevel(true)
            .addServiceUuid(mCurrentServiceFragment.getServiceUUID())
            .build()
        mAdvScanResponse = AdvertiseData.Builder()
            .setIncludeDeviceName(true)
            .build()
    }

    companion object {

        private val REQUEST_ENABLE_BT = 1
        private val TAG: String =
            MainActivity::class.java.getCanonicalName()
        private val CURRENT_FRAGMENT_TAG = "CURRENT_FRAGMENT"

        private val CHARACTERISTIC_USER_DESCRIPTION_UUID = UUID
            .fromString("00002901-0000-1000-8000-00805f9b34fb")
        private val CLIENT_CHARACTERISTIC_CONFIGURATION_UUID = UUID
            .fromString("00002902-0000-1000-8000-00805f9b34fb")

        ///////////////////////
        ////// Bluetooth //////
        ///////////////////////
        fun getClientCharacteristicConfigurationDescriptor(): BluetoothGattDescriptor? {
            val descriptor = BluetoothGattDescriptor(
                CLIENT_CHARACTERISTIC_CONFIGURATION_UUID,
                BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
            )
            descriptor.value = byteArrayOf(0, 0)
            return descriptor
        }

        fun getCharacteristicUserDescriptionDescriptor(defaultValue: String): BluetoothGattDescriptor? {
            val descriptor = BluetoothGattDescriptor(
                CHARACTERISTIC_USER_DESCRIPTION_UUID,
                BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
            )
            try {
                descriptor.value = defaultValue.toByteArray(charset("UTF-8"))
            } finally {
                return descriptor
            }
        }
    }

    private fun ensureBleFeaturesAvailable() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "BLE Not supported", Toast.LENGTH_LONG).show()
            Log.e(
                MainActivity.TAG,
                "Bluetooth not supported"
            )
            finish()
        } else if (!mBluetoothAdapter!!.isEnabled) {
            // Make sure bluetooth is enabled.
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableBtIntent,
                REQUEST_ENABLE_BT
            )
        }
    }

    override fun onStart() {
        super.onStart()
        // If the user disabled Bluetooth when the app was in the background,
        // openGattServer() will return null.
        // If the user disabled Bluetooth when the app was in the background,
        // openGattServer() will return null.
        mGattServer = mBluetoothManager.openGattServer(this, mGattServerCallback)
        if (mGattServer == null) {
            ensureBleFeaturesAvailable()
            return
        }
        // Add a service for a total of three services (Generic Attribute and Generic Access
        // are present by default).
        // Add a service for a total of three services (Generic Attribute and Generic Access
        // are present by default).
        mGattServer!!.addService(mBluetoothGattService)

        if (mBluetoothAdapter!!.isMultipleAdvertisementSupported) {
            mAdvertiser = mBluetoothAdapter!!.bluetoothLeAdvertiser
            mAdvertiser!!.startAdvertising(mAdvSettings, mAdvData, mAdvScanResponse, mAdvCallback)
        }
    }

    private val mAdvCallback: AdvertiseCallback = object : AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            super.onStartFailure(errorCode)
            Log.e(
                TAG,
                "Not broadcasting: $errorCode"
            )
            val statusText: Int
            when (errorCode) {
                ADVERTISE_FAILED_ALREADY_STARTED -> {
                    statusText = R.string.already_advertising
                    Log.w(
                        TAG,
                        "App was already advertising"
                    )
                }
                ADVERTISE_FAILED_DATA_TOO_LARGE -> statusText = R.string.status_advDataTooLarge
                ADVERTISE_FAILED_FEATURE_UNSUPPORTED -> statusText =
                    R.string.status_advFeatureUnsupported
                ADVERTISE_FAILED_INTERNAL_ERROR -> statusText = R.string.status_advInternalError
                ADVERTISE_FAILED_TOO_MANY_ADVERTISERS -> statusText =
                    R.string.status_advTooManyAdvertisers
                else -> {
                    statusText = R.string.status_notAdvertising
                    Log.wtf(
                        TAG,
                        "Unhandled error: $errorCode"
                    )
                }
            }
            Toast.makeText(this@MainActivity, getString(statusText), Toast.LENGTH_SHORT).show()
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            super.onStartSuccess(settingsInEffect)
            Log.v(TAG, "Broadcasting")
            Toast.makeText(this@MainActivity, getString(R.string.status_advertising), Toast.LENGTH_SHORT).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                if (!mBluetoothAdapter!!.isMultipleAdvertisementSupported) {
                    Toast.makeText(
                        this,
                        "Bluetooth Advertising not supported",
                        Toast.LENGTH_LONG
                    ).show()
                    Log.e(
                        TAG,
                        "Advertising not supported"
                    )
                }
                onStart()
            } else {
                //TODO(g-ortuno): UX for asking the user to activate bt
                Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show()
                Log.e(
                    TAG,
                    "Bluetooth not enabled"
                )
                finish()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGattServer != null) {
            mGattServer!!.close()
        }
        if (mBluetoothAdapter!!.isEnabled && mAdvertiser != null) {
            // If stopAdvertising() gets called before close() a null
            // pointer exception is raised.
            mAdvertiser!!.stopAdvertising(mAdvCallback)
        }
//        resetStatusViews()
    }

    override fun sendNotificationToDevices(characteristic: BluetoothGattCharacteristic) {
        val indicate = ((characteristic.properties
                and BluetoothGattCharacteristic.PROPERTY_INDICATE)
                == BluetoothGattCharacteristic.PROPERTY_INDICATE)
        for (device in mBluetoothDevices) {
            // true for indication (acknowledge) and false for notification (unacknowledge).
            mGattServer!!.notifyCharacteristicChanged(device, characteristic, indicate)
        }
    }
}